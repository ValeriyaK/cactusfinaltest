package Tests;

import Pages.*;
import net.bytebuddy.pool.TypePool;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.ImplicitlyWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class HeaderTest extends BaseTest {
    @Test
    public void guarantee() {
        CactusShopPage cactusShopPage = new Header(driver)
                .toCactusShop();
        Assert.assertTrue(cactusShopPage.cactusShop.isDisplayed());
    }

    @Test
    public void appleLink() {
        ProductPage productPage = new Header(driver)
                .toappleLink();
        WebElement element = driver.findElement(By.xpath("//span[text()= 'Код  08298']"));
        Assert.assertTrue(productPage.productId.isDisplayed());
    }

    @Test
    public void basketTest() {
        MainPage mainPage = new MainPage(driver)
                .clickOnBuyButton()
                .clickOnContinueShoppingButton();
        driver.navigate().refresh();
        Header header = new Header(driver);
        Assert.assertTrue(header.amountProductBasket.isDisplayed());
    }

    @Test
    public void watchTest() {
        ProductGroupPage productPage = new Header(driver)
                .toWatchPage();
        Assert.assertTrue(productPage.appleWatchGroup.isDisplayed());
    }

    @Test
    public void appleWatchDropdown() throws InterruptedException {
        Header header = new Header(driver);
        ProductGroupPage productGroupPage = new ProductGroupPage(driver);
        new Actions(driver).moveToElement(header.watch).perform();
        header.appleWatchDropdown.click();
        Assert.assertTrue(productGroupPage.appleWatchSideBar.isDisplayed());
    }

    @Test
    public void deletingFromBasket() {
        MainPage mainPage = new MainPage(driver)
                .clickOnBuyButton()
                .clickOnContinueShoppingButton();
        driver.navigate().refresh();
        Header header = new Header(driver)
                .deleteFromBasket();
        Assert.assertTrue(header.amountProductBasket.isDisplayed());

    }

    @Test
    public void meizuPhoneTest() {
        ProductGroupPage productGroupPage = new Header(driver)
                .clickOnMeizuPhone();
        Assert.assertTrue(productGroupPage.titleMeizuPhone.isDisplayed());
    }

    @Test
    public void slider() {
        WebElement explicitWait = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@style='left: -16.6667%;']")));
    }

    @Test
    public void cactus() {
        Header header = new Header(driver);
        header.clickToLogin();
        header.clickOnCactus();
        Assert.assertTrue(header.cactus.isDisplayed());
    }
}

