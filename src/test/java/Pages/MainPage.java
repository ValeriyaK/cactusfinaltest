package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import javax.swing.*;

public class MainPage extends BasePage {

    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[text()='Личный кабинет']")
    public WebElement personalCabinet;

    @FindBy(xpath = "//a[@title='Вход в личный кабинет']")
    public WebElement moveToLogin;

    @FindBy(xpath = "//span[text()='Купить']")
    public WebElement buyButton;

    @FindBy(xpath = "//span[@title='Продолжить покупки']")
    public WebElement continueShopping;

    @FindBy(xpath = "//img[@src='https://cactus.r.worldssl.net/18121-home_default/naushniki-huawei-freebuds-2-pro-cm-h2-white.jpg']/..")
    public WebElement producOnMainPage;

    @FindBy(xpath = "//a[@data-id-product='9926'][@title='Сравнить']/..")
    public WebElement comparsionIcon;

    @FindBy(xpath = "//a [@class='fancybox-item fancybox-close']")
    public WebElement fancyBox;

    @FindBy(xpath = "//a[@data-id-product='9922']")
    private WebElement comparsionIcon2;

    @FindBy(xpath = "//a[@href='/products-comparison']")
    public WebElement comparsionProduct;


    @Step
    public AuthPage getAuthPage() {
        personalCabinet.click();
        return new AuthPage(driver);
    }

    @Step
    public AuthPage clickToLogin() {
        moveToLogin.click();
        return new AuthPage(driver);

    }

    @Step
    public MainPage clickOnBuyButton() {
        buyButton.click();
        driver.findElement(By.xpath("//span[text()='Купить']"));
        return this;
    }

    @Step
    public MainPage clickOnContinueShoppingButton() {
        continueShopping.click();
        return this;
    }

    @Step
    public ProductDetails clickOnProduct() {
        producOnMainPage.click();
        return new ProductDetails(driver);

    }

    @Step
    public MainPage clickOncompassionButton() {
        comparsionIcon.click();
        return this;

    }

    @Step

    public MainPage clickOnFuncyBoxClose() {
        fancyBox.click();
        return this;

    }

    @Step

    public MainPage clickOnComparsionButton2() {
        WebElement explicitWait = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@data-id-product='9922']")));
        comparsionIcon2.click();
        return this;

    }

    @Step
    public ComparsionPage moveToCompersionPage() {
        comparsionProduct.click();
        return new ComparsionPage(driver);
    }

}
