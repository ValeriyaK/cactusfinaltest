package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductDetails extends BasePage {
    public ProductDetails(WebDriver webDriver) {

        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='credit creditonline']")
    public WebElement creditOnline;

    @FindBy(xpath = "//div[@id='preCreditWindow']")
    public WebElement preCreditWindow;

    @FindBy(xpath = "//span[text()= 'Код  09118']")
    public WebElement meizuTitleCode;

    @FindBy(xpath = "//img[@id='bigpic']")
    public WebElement zoomProduct;

    @FindBy(xpath = "//img[@src='https://cactus.r.worldssl.net/18121-thickbox_default/naushniki-huawei-freebuds-2-pro-cm-h2-white.jpg']")
    public WebElement zoomImg;

    @FindBy(xpath = "//a[@class='fancybox-item fancybox-close']")
    public WebElement сloseZoom;

    @Step
    public ProductDetails zoomProduct() {
        zoomProduct.click();
        return this;
    }

    @Step
    public ProductDetails closeZoom() {
        сloseZoom.click();
        return this;
    }

    @Step
    public ProductDetails creditOnline() {
        creditOnline.click();
        return this;
    }

}
