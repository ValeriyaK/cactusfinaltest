package Pages;


import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header extends BasePage {
    public Header(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[@title='Вход в личный кабинет']")
    public WebElement moveToLogin;

    @FindBy(xpath = "//input[@id='search_query_block']")
    public WebElement searchField;

    @FindBy(xpath = "//button[@id='search_button']")
    public WebElement searchButton;

    @FindBy(xpath = "//label[@for='search_query_block']/a[1]")
    public WebElement fastSearch;

    @FindBy(xpath = "//a[@title='Выход']")
    public WebElement logout;

    @FindBy(xpath = "//a[@title='Магазин Cactus']")
    public WebElement cactusShop;

    @FindBy(xpath = "//a[@id='toplink01']")
    public WebElement appleLink;

    @FindBy(xpath = "//img[@title='Корзина']")
    public WebElement basket;

    @FindBy(xpath = "//span[@class='cart-items-count count ajax_cart_quantity'][text()='1']")
    public WebElement amountProductBasket;

    @FindBy(xpath = "//a[@title='Часы'][@style]")
    public WebElement watch;

    @FindBy(xpath = "//div[@class='menu-container']/ul/li[3]/ul/li[1]/a")
    public WebElement appleWatchDropdown;

    @FindBy(xpath = "//a[@title='удалить товар из корзины']")
    public WebElement deleteFromBasket;

    @FindBy(xpath = "//a[@title='Смартфоны от Meizu']")
    public WebElement meizePhone;

    @FindBy(xpath = "//a[@href='https://cactus.kh.ua/products-comparison']")
    public WebElement comparison;

    @FindBy(xpath = "//img[@title='Cactus - гаджеты и аксессуары']")
    public WebElement cactus;

    @FindBy(xpath = "//img[@src = '//cactus.r.worldssl.net/modules/jscomposer/uploads/xiaomi-2.jpg']")
    public WebElement slider;


    @Step
    public AuthPage clickToLogin() {
        moveToLogin.click();
        return new AuthPage(driver);
    }

    @Step
    public Result positiveSearch(String str) {
        searchField.sendKeys(str);
        searchButton.click();
        return new Result(driver);
    }

    @Step
    public Result negativeSearch(String str) {
        searchField.sendKeys(str);
        searchButton.click();
        return new Result(driver);
    }

    @Step
    public Result fastSearch() {
        fastSearch.click();
        return new Result(driver);
    }

    @Step
    public AuthPage logout() {
        logout.click();
        return new AuthPage(driver);

    }

    @Step
    public CactusShopPage toCactusShop() {
        cactusShop.click();
        return new CactusShopPage(driver);
    }

    @Step
    public ProductPage toappleLink() {
        appleLink.click();
        return new ProductPage(driver);
    }

    @Step
    public ProductGroupPage toWatchPage() {
        watch.click();
        return new ProductGroupPage(driver);
    }

    @Step
    public Header deleteFromBasket() {
        basket.click();
        deleteFromBasket.click();
        return this;
    }

    @Step
    public ProductGroupPage clickOnMeizuPhone() {
        meizePhone.click();
        return new ProductGroupPage(driver);
    }

    @Step
    public ComparsionPage clickOnComparsionButton() {
        comparison.click();
        return new ComparsionPage(driver);
    }

    @Step
    public Header clickOnCactus() {
        cactus.click();
        return this;
    }


}




