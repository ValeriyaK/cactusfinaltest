package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CactusShopPage extends BasePage {

    public CactusShopPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//img[@src='https://cactus.kh.ua/img/cms/mn1.jpg']")
    public WebElement cactusShop;
}
